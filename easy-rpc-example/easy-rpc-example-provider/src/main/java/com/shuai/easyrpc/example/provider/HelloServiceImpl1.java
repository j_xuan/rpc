package com.shuai.easyrpc.example.provider;

import com.shuai.easyrpc.annotation.ServiceExpose;
import com.shuai.easyrpc.example.provider.api.HelloService;

/**
 * Hello World
 * 服务提供者，使用@ServiceExpose注解对外暴露服务
 *
 * @author 小猴子
 * @since 2021/11/29
 */
//@ServiceExpose     //测试为啥放不了多个实现类
public class HelloServiceImpl1 implements HelloService {
    public String sayHello(String name) {
        return "123";
    }
}
