# easy-RPC 框架

## easy-RPC是一款低性能的服务框架，主要用于学习造轮子！

学习本项目可以让你从零开始实现一个类似 Dubbo 服务框架 mini 版RPC。

## 具体解析可以参照本人博客：[从零开始实现一个分布式RPC框架](https://www.cnblogs.com/monkey-xuan/p/15893604.html)

如果你认真学下来，可以掌握以下的技术：

1. 底层网络层基于 netty，学完 netty 入门没有问题；
2. 使用自定义注解，学完可以了解注解的基本运行机制；
3. 服务注册基于 zookeeper，学完 zk 入门没有问题；
4. 会用到反射机制；
5. 会用到动态代理技术；
6. 教你如何定义一个 xxx-spring-boot-starter，了解spring boot自动配置机制；
7. 学会如何自定义配置项，并绑定到 bean；
8. 学习监听 spring 容器的事件；
9. ……等等

有没有一点心动呢？！

# 快速开始

## 环境准备

- JDK8 或以上
- Maven 3
- Zookeeper 单机或者集群实例
- IntelliJ IDEA

## 编译安装源码


> 敲黑板：以下指导文档涉及到的演示代码已存放在easy-rpc-example这个目录下。

下载源码

```bash
git clone https://gitee.com/j_xuan/rpc.git
```

编译安装 jar 包到本地仓库 

```bash
mvn clean install
```



## 服务端配置

### 暴露接口

定义一个服务接口

```java
/**
 * Hello World
 *
 * @author 小帅
 * @since 2021/11/29
 */
public interface HelloService {
    /**
     * 打招呼
     *
     * @param name 名称
     * @return 问候语
     */
    String sayHello(String name);
}
```

实现接口，使用自定义注解@ServiceExpose 暴露一个服务接口

```java
/**
 * Hello World
 *
 * @author 小猴子
 * @since 2021/11/29
 */
@ServiceExpose
public class HelloServiceImpl implements HelloService {
    public String sayHello(String name) {
        return "「来自小帅的问候」：hello " + name + " , 恭喜你学会了造RPC轮子！";
    }
}
```

### 配置注册中心地址

当前项目只支持 zookeeper 作为注册中心。服务端（provider）使用 zookeeper 为了暴露服务接口。

```properties
# application.properties

# rpc 服务暴露的端口
shuai.easy.rpc.expose-port=6666
# zookeeper 注册中心地址
shuai.easy.rpc.zk-address=127.0.0.1:2181
```

## 客户端配置

### 注入远端服务

使用自定义注解 @ServiceReference 自动注入服务端暴露的接口服务

```java
/**
 * @author 小猴子
 * @since 2021/12/1
 */
@RestController
public class HelloController {
    private static final Logger logger = LoggerFactory.getLogger(HelloController.class);

    @ServiceReference
    private HelloService helloService;

    @GetMapping("/hello/{name}")
    public String hello(@PathVariable String name) {
        final String rsp = helloService.sayHello(name);
        logger.info("Receive message from rpc server, msg: {}", rsp);
        return rsp;
    }
}
```

### 配置注册中心地址

客户端配置 zookeeper 是为了订阅发现服务端暴露的服务接口


```properties
# application.properties

# zookeeper 实例地址
shuai.easy.rpc.zk-address=127.0.0.1:2181
```

## 启动测试

### 运行服务端（服务提供者）

```java
/**
 * 服务提供者启动入口
 *
 * @author 小猴子
 * @since 2021/11/29
 */
@SpringBootApplication
public class ProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class, args);
    }
}
```

### 运行客户端（服务消费者）

```java
/**
 * 服务消费者启动入口
 * 
 * @author 小猴子
 * @since 2021/12/01
 */
@SpringBootApplication
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }
}
```

### 测试

使用浏览器输入请求地址测试：

```text
http://127.0.0.1:8081/hello/小帅
```

返回下面的字符串就说明运行成功
```text
「来自小猴子的问候」：hello 小帅 , 恭喜你学会了造RPC轮子！"
```

