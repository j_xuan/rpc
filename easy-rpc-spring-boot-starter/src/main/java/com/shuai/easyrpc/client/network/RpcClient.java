package com.shuai.easyrpc.client.network;

import com.shuai.easyrpc.common.ServiceInfo;

/**
 * @Description 网络客户端：client侧
 * @Author 小猴子
 * @Date 2021/11/24
 */
public interface RpcClient {
    /**
     *
     * @param data 待发送的消息
     * @param serviceInfo 消息接收者
     * @return 已发送消息
     */
    byte[] sendMessage(byte[] data, ServiceInfo serviceInfo) throws InterruptedException;
}
