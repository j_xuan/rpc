package com.shuai.easyrpc.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 定义注解：标识服务提供者，暴露服务接口
 * @Author 小猴子
 * @Date 2021/11/25
 */
@Component
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceExpose {
    //设置注解参数,默认为空
    String value() default "";
}
