package com.shuai.easyrpc.exception;

/**
 * @author 小猴子
 * @since 2021/11/30
 */
public class RpcException extends RuntimeException{
    public RpcException(String message) {
        super(message);
    }
}
