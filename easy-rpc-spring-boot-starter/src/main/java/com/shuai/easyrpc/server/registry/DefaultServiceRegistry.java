package com.shuai.easyrpc.server.registry;

import com.shuai.easyrpc.common.ServiceInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * 本地服务注册器
 *
 * @author 小猴子
 * @since 2021/11/25
 */
public class DefaultServiceRegistry implements ServiceRegistry {
    protected String protocol;
    protected Integer port;
    private Map<String, ServiceInfo> serviceInfoHashMap = new HashMap<>();

    @Override
    public void register(ServiceInfo serviceInfo) throws Exception {
        if (serviceInfo == null) {
            throw new IllegalArgumentException("param.invalid");
        }
        final String name = serviceInfo.getServiceName();
        serviceInfoHashMap.put(name, serviceInfo);
    }

    @Override
    public ServiceInfo getServiceInstance(String name) throws Exception {
        return serviceInfoHashMap.get(name);
    }
}
