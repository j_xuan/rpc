package com.shuai.easyrpc.config;

import com.shuai.easyrpc.client.ClientProxyFactory;
import com.shuai.easyrpc.client.discovery.ServiceDiscovery;
import com.shuai.easyrpc.client.discovery.ZookeeperServiceDiscovery;
import com.shuai.easyrpc.client.network.NettyRpcClient;
import com.shuai.easyrpc.listener.DefaultRpcListener;
import com.shuai.easyrpc.property.RpcProperties;
import com.shuai.easyrpc.serialization.DefaultMessageProtocol;
import com.shuai.easyrpc.server.network.NettyRpcServer;
import com.shuai.easyrpc.server.network.RequestHandler;
import com.shuai.easyrpc.server.network.RpcServer;
import com.shuai.easyrpc.server.registry.ServiceRegistry;
import com.shuai.easyrpc.server.registry.ZookeeperServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 小猴子
 * @since 2021/11/28
 */
@Configuration
public class AutoConfiguration {

    // 监听器
    @Bean
    public DefaultRpcListener defaultRpcListener(@Autowired ServiceRegistry serviceRegistry,
                                                 @Autowired RpcServer rpcServer,
                                                 @Autowired ClientProxyFactory clientProxyFactory,
                                                 @Autowired RpcProperties rpcProperties) {
        return new DefaultRpcListener(serviceRegistry, rpcServer, clientProxyFactory, rpcProperties);
    }

    // 配置属性
    @Bean
    public RpcProperties rpcProperty() {
        return new RpcProperties();
    }

    // 客户端
    @Bean
    public ServiceDiscovery serviceDiscovery(@Autowired RpcProperties rpcProperties) {
        return new ZookeeperServiceDiscovery(rpcProperties.getZkAddress());
    }

    @Bean
    public ClientProxyFactory clientProxyFactory(@Autowired ServiceDiscovery serviceDiscovery) {
        return new ClientProxyFactory(serviceDiscovery, new DefaultMessageProtocol(), new NettyRpcClient());
    }

    // 服务端
    @Bean
    public ServiceRegistry serviceRegister(@Autowired RpcProperties rpcProperties) {
        return new ZookeeperServiceRegistry(rpcProperties.getZkAddress(), rpcProperties.getExposePort(),
                rpcProperties.getProtocol());
    }

    @Bean
    public RequestHandler requestHandler(@Autowired ServiceRegistry serviceRegistry) {
        return new RequestHandler(new DefaultMessageProtocol(), serviceRegistry);
    }

    @Bean
    public RpcServer rpcServer(@Autowired RpcProperties rpcProperties,
                               @Autowired RequestHandler requestHandler) {
        final Integer port = rpcProperties.getExposePort();
        final String protocol = rpcProperties.getProtocol();
        return new NettyRpcServer(port, protocol, requestHandler);
    }

}
